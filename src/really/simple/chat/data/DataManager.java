/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package really.simple.chat.data;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.Server;

/**
 *
 * @author graphene
 */


public class DataManager {
    private static DataManager instance;
    private static Connection connection;
    private static Statement statement;
    
    private DataManager() {
        try {
            SecureRandom random = new SecureRandom();
            
            Server server = Server.createTcpServer().start();
            JdbcDataSource dataSource = new JdbcDataSource ();
            dataSource.setURL ("jdbc:h2:tcp://localhost/~/rsc");
            dataSource.setUser ("server");
            byte[] bytes = {};
            random.nextBytes(bytes);
            dataSource.setPassword (Arrays.toString(bytes));
            
            connection = dataSource.getConnection ();
            statement = connection.createStatement ();
            try {
                createTable();
            } catch (SQLException e) {
                erasePreviousTable();
                createTable();
            }
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
        
    }

    private void createTable() throws SQLException {
        String creator = "create table USERS (" +
                "UUID varchar NOT NULL, " +
                "UserName varchar, " +
                "PRIMARY KEY (UUID))";
        
        statement.executeUpdate(creator);
    }
    
    private void erasePreviousTable () throws SQLException {
        String creator = "drop table USERS";
        statement.executeUpdate (creator);
    }
    
    public static DataManager getInstance () {
        if (instance == null)
            instance = new DataManager();
        
        return instance;
    }
    
    public List <UserData> getAllUsers () {
        List <UserData> userList = null;
        try {
            ResultSet result = statement.executeQuery ("SELECT * FROM USERS");
            userList = new ArrayList <UserData> ();
            
            while (result.next ()) {
                UUID id = UUID.fromString (result.getString ("UUID")) ;
                String name = result.getString ("UserName");
                userList.add (new UserData (name, id));
            }            
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
        return userList;
    }
    
    public void put (UserData... datas) {
        try{
            PreparedStatement prepared = connection.prepareStatement(
                "INSERT INTO USERS(UserName,UUID) VALUES(?,?)");
            for (UserData data : datas) {
                prepared.setString (1, data.getName ());
                prepared.setString (2, data.getId ().toString ());
                prepared.execute ();
            }
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
    }
    
    public void change (UserData... datas) {
        try {
            PreparedStatement prepared = connection.prepareStatement (
               "UPDATE USERS SET UserName=? WHERE UUID=?");
            for (UserData data : datas) {
                prepared.setString (1, data.getName());
                prepared.setString (2, data.getId().toString());
                prepared.execute ();
            }
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
    }
    
    public void delete (UserData... datas) {
        try {
            for (UserData data : datas)
                statement.execute ("DELETE FROM USERS WHERE UUID=" +
                    data.getId ().toString ());
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
    }
    
    public void deleteAll () {
        try {
            statement.execute ("DELETE FROM USERS");
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
    }
    
    public List <UserData> getByUUID (UUID... uuids) {
        List <UserData> users = new ArrayList <UserData> ();
        try {
            for (UUID uuid : uuids){
                ResultSet result = statement.executeQuery ("SELECT * FROM USERS WHERE UUID='" +
                    uuid.toString () + "'");
                if (result.next ()) {
                    String name = result.getString ("UserName");
                    users.add (new UserData (name, uuid));
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
        return users;
    }
    
    public boolean exists (UserData data) {
        boolean exists = false;
        try {
            ResultSet result = statement.executeQuery ("SELECT * FROM USERS WHERE UUID='" +
                data.getId ().toString () + "'");
            exists = result.next ();
        } catch (SQLException ex) {
            Logger.getLogger (DataManager.class.getName ())
                .log (Level.SEVERE, null, ex);
        }
        return exists;
    }
}
