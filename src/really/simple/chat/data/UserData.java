/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package really.simple.chat.data;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author graphene
 */


public class UserData implements Serializable{
    private static final long serialVersionUID = 0L ;
    
    private String userName;
    private final UUID id;
    
    public UserData () {
        id = UUID.randomUUID ();
    }
    
    public UserData (String userName) {
        this ();
        this.userName = userName;
    }
    
    public UserData (String userName, UUID uuid) {
        this.userName = userName;
        id = uuid;
        
    }
    
    /**
     * @return the user name
     */
    public String getName() {
        return userName;
    }

    /**
     * @param name the user name to set
     */
    public void setName (String name) {
        userName = name;
    }

    /**
     * @return the id
     */
    public UUID getId () {
        return id;
    }
    
    @Override
    public boolean equals (Object obj){
        if (obj == null || obj.getClass () != getClass ())
            return false;
        UserData compare = (UserData) obj;
        
        return id.equals (compare.getId ()) ;
    }
    
    @Override
    public int hashCode () {
        return id.hashCode () ;
    }
}
