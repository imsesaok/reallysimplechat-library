/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package really.simple.chat.data.message;

import java.io.Serializable;
import java.util.Date;

import really.simple.chat.data.UserData;

/**
 *
 * @author owner
 */


public class Message implements Serializable {
    private static final long serialVersionUID = 0L ;
    
    private final UserData user;
    private final Serializable data;
    private final Date date;
    private final MessageType type;
    
    public Message (Date date, UserData user, MessageType type, Serializable data) {
        if (date != null)
            this.date = date;
        else
            this.date = new Date();
        
        this.user = user;
        
        if (type != null)
            this.type = type;
        else
            this.type = MessageType.ETC;
        
        this.data = data;
    }
    
    /**
     * @return the user's data.
     */
    public UserData getUserData () {
        return user;
    }
    /**
     * @return the date instance.
     */
    public Date getDate () {
        return date;
    }
    /**
     * @return the message type.
     */
    public MessageType getMessageType () {
        return type;
    }
    
    /**
     * @return the content.
     */
    public Serializable getContent() {
        return data;
    }
}
