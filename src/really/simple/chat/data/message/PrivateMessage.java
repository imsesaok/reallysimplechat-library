/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package really.simple.chat.data.message;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import really.simple.chat.data.UserData;

/**
 *
 * @author graphene
 */


public class PrivateMessage extends Message{
    
    private final List <UserData> recipients;

    public PrivateMessage (Date date, UserData from, MessageType type, Serializable data, UserData... to) {
        super (date, from, type, data);
        recipients = Arrays.asList (to);
    }
    
    public List <UserData> getRecipients () {
        return recipients;
    }
}
