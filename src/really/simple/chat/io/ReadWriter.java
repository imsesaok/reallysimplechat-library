/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package really.simple.chat.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import really.simple.chat.data.message.Message;

/**
 *
 * @author Graphene
 */
public class ReadWriter {
     private final Map <SocketChannel, List<ByteBuffer>> writeLater;
     private static final Logger logger = Logger.getLogger (ReadWriter.class.getName());
     
     public ReadWriter () {
        writeLater = new HashMap <SocketChannel, List<ByteBuffer>> ();
     }
     
     public Message read (SocketChannel channel) {
        ByteBuffer buffer = ByteBuffer.allocate (1024); 
        buffer.clear ();

        try {
            if (channel.read (buffer) < 0) {
                channel.close ();
                writeLater.remove (channel);
            } else {
                buffer.flip ();
                Message received = MessageByteBufferConverter.toMessage (buffer);
                System.out.println("Successfully read!");
                return received;
            }
        } catch (IOException ex) {
           logger.log (Level.SEVERE, "Unexpected Connection Lost.", ex);

           writeLater.remove (channel);
           try {
               channel.close ();
           } catch (IOException ex1) {
               logger.log (Level.SEVERE, null, ex1);
           }
        }
        return null;
    }
    
    public boolean write (SocketChannel channel, Message message) throws IOException {        
        ByteBuffer data = MessageByteBufferConverter.toByteBuffer (message) ;
        if (channel.write (data) <= 0) {
            addForFuture (channel, message) ;
            System.out.println("Not written, will try to write in then future.");
            return false;
        } else {
            System.out.println("Successfully Written!");
            return true;
        }
    }
    public void addForFuture (SocketChannel channel, Message message) {
        ByteBuffer data = MessageByteBufferConverter.toByteBuffer (message) ;
        
        if (writeLater.containsKey (channel)) {
            writeLater.get (channel).add (data) ;
        } else {
            List <ByteBuffer> list = new ArrayList <ByteBuffer> ();
            list.add (data);
            writeLater.put (channel, list);
            System.out.println("Added For Future!");
        }
    }
    
    public boolean rewrite (SocketChannel channel) {
        if (! writeLater.containsKey (channel))
            return false; //nothing to rewrite
        
        List <ByteBuffer> buffers = writeLater.get (channel);
        
        if (buffers.isEmpty ()) {
            writeLater.remove (channel);
            return false;
        }
        for (ByteBuffer data : buffers) {
            try {
                if (channel.write (data) >= 0) {
                    buffers.remove (data);
                    System.out.println("Successfully Written!");
                } else {
                    System.out.println("Not written, will try to write in then future.");
                    return false;
                }
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Cannot write to socket!", ex);
            }
        }
        return true;
    }
}
