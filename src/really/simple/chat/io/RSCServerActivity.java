/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package really.simple.chat.io;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import really.simple.chat.event.ActionListener;
import really.simple.chat.data.UserData;
import really.simple.chat.data.message.Message;
import really.simple.chat.data.message.MessageType;
import really.simple.chat.data.message.PrivateMessage;
import really.simple.chat.event.EventListener;
import really.simple.chat.event.EventType;
/**
 *
 * @author owner
 */
public class RSCServerActivity extends RSCBaseServerActivity {
    String serverName;
    final Map <MessageType, List <ActionListener>> listenerMap = new EnumMap
        <MessageType, List <ActionListener>> (MessageType.class);
    final Map <EventType, List<EventListener>> eventListenerMap = new EnumMap
        <EventType, List<EventListener>> (EventType.class);
    List <String> userList1 = new ArrayList <String> ();
    boolean isPrivate = false;
    private static final Logger logger = Logger.getLogger (RSCServerActivity.class.getName());
    
    ActionListener renameListener = new ActionListener() {
        public void process (Message msg) {
            
        }
    };
    
    public RSCServerActivity (int port) {
        super (port, new ReadWriter()) ;
        serverName = "" ;
        addMessageListener (MessageType.RN, renameListener);
    }
    
    public RSCServerActivity (String name, int port, boolean isPrivate) {
        this (port) ;
        serverName = name;
        this.isPrivate = isPrivate;
    }

    @Override
    boolean accept (SocketChannel client, Selector selector) throws IOException {
        client.configureBlocking (false);
        
        if (! isEligible (client)) {
            client.finishConnect ();
            selector.close ();
            return false;
        }
        
        if (client.isConnectionPending ()) client.finishConnect();
        
        invokeEvent (EventType.Connected) ;
        client.register (selector, SelectionKey.OP_READ) ;
        return true;
    }    
    
    @Override
    protected void processAndWrite (Message message) throws IOException {
        if (listenerMap.containsKey (message.getMessageType ())) {
            for (ActionListener listener : listenerMap.get (message.getMessageType ())) {
                if (listener != null) {
                    listener.process (message);
                }
            }
        }
        
        if (message instanceof PrivateMessage) {
            PrivateMessage prvMessage = (PrivateMessage) message;
            this.writeNow(selector, message, prvMessage.getRecipients ());
        } else this.writeNowToEveryone(selector, message);
    }

    /**
     * Checks if the client is eligible to enter.
     * @param channel
     * @return eligibility of the client.
     * @throws IOException
     */
    public boolean isEligible (SocketChannel channel) throws IOException {
        boolean isEligible = false;
        if (isPrivate) { //whitelist
            if (userList1.contains (channel.getRemoteAddress ().toString ()))
                isEligible = true;
        } else { //blacklist
            if (! userList1.contains (channel.getRemoteAddress ().toString ()))
                isEligible = true;
        }
        return isEligible;
    }
    
    public void ban (SocketChannel channel) throws IOException {
        channel.close ();
        userList1.add (channel.getRemoteAddress ().toString ());
    }
    
    public final void addMessageListener (MessageType type, ActionListener... listeners) {
        List <ActionListener> listenerList = listenerMap.get (type);
        if (listenerList == null)
            listenerList = new ArrayList <ActionListener> ();
        listenerList.addAll (Arrays.asList (listeners));
        listenerMap.put (type, listenerList);
    }
    
    public final void addEventListener (EventType type, EventListener... listeners){
        List <EventListener> listenerList = eventListenerMap.get (type);
        if (listenerList == null)
            listenerList = new ArrayList <EventListener> ();
        listenerList.addAll (Arrays.asList (listeners));
        eventListenerMap.put (type, listenerList);
    };
    
    public final List <ActionListener> getMessageListener (MessageType type) {
        return listenerMap.get (type) ;
    }
    
    public final List <EventListener> getEventListener (EventType type) {
        return eventListenerMap.get (type) ;
    }
    
    public final List <ActionListener> removeAllMessageListener (MessageType type) {
        return listenerMap.remove (type);
    }
    
    public final List <EventListener> removeAllEventListener (EventType type) {
        return eventListenerMap.remove (type);
    }
    
    public final boolean removeMessageListener (MessageType type, ActionListener... listeners) {
        List <ActionListener> listenerList = listenerMap.get (type);
        boolean succeed = false;
        if (listenerList != null) {
            listenerList = new ArrayList <ActionListener> ();
            succeed = listenerList.removeAll (Arrays.asList (listeners));
            listenerMap.put (type, listenerList);
        }
        return succeed;
    }
    
    public final boolean removeEventListener (EventType type, EventListener... listeners) {
        List <EventListener> listenerList = eventListenerMap.get (type);
        boolean succeed = false;
        if (listenerList != null) {
            listenerList = new ArrayList <EventListener> ();
            succeed = listenerList.removeAll (Arrays.asList (listeners));
            eventListenerMap.put (type, listenerList);
        }
        return succeed;
    }

    @Override
    protected void invokeEvent(EventType type) {
        if (eventListenerMap.containsKey (type)) {
            for (EventListener listener : eventListenerMap.get(type)) {
                if (listener != null) {
                    listener.process();
                }
            }
        }
    }
}
