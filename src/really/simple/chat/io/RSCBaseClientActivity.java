/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package really.simple.chat.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketOption;
import java.net.StandardSocketOptions;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import really.simple.chat.data.UserData;

import really.simple.chat.data.message.Message;
import really.simple.chat.data.message.MessageType;
import really.simple.chat.event.EventType;
import static really.simple.chat.io.RSCBaseServerActivity.serverIdentity;

/**
 * RSC Base Socket. Contains the core of the client (the socket loop, data management, etc.).
 * @author Graphene
 */
public abstract class RSCBaseClientActivity implements Runnable {
    private SocketAddress address;
    boolean isRunning = false;
    final ReadWriter rw;
    protected Selector selector;
    protected SocketChannel channel;
    private static final Logger logger = Logger.getLogger (RSCBaseClientActivity.class.getName());
    String userName;
    
    public RSCBaseClientActivity (SocketAddress address, ReadWriter rw) {
        this.address = address;
        this.rw = rw;
    }
    
    public RSCBaseClientActivity (String ip, int port, ReadWriter rw) {
        this (new InetSocketAddress (ip, port), rw);
    }
    
    @Override
    @SuppressWarnings ("empty-statement")
    public void run () {
        try {
            channel = SocketChannel.open (address);
            selector = Selector.open ();
            initialize (channel);
            channel.register (selector, SelectionKey.OP_CONNECT);
            isRunning = true;
            
            while (! channel.finishConnect ());
            invokeEvent (EventType.Connected);
            
            channel.register (selector, SelectionKey.OP_READ);
            
            while (isRunning) {
                try {
                    if (isRunning == false) invokeEvent (EventType.Disconnected);
                    
                    selector.selectNow ();
                    for (SelectionKey key : selector.selectedKeys ()) {
                        selector.selectedKeys ().remove (key) ;
                        SocketChannel server = (SocketChannel) key.channel ();
                        
                        if (key.isReadable ()) {
                            Message data = rw.read (server) ;
                            if (data == null) {
                                logger.log (Level.WARNING, "Unexpected Connection Lost!") ;
                                invokeEvent (EventType.Disconnected);
                            } else process (data) ;
                        }
                        if (key.isWritable ()){
                            if (rw.rewrite (server)) reRegister (SelectionKey.OP_READ);
                        }
                    }
                } catch (IOException e) {
                    logger.log (Level.SEVERE, "Problem while operating client,", e);
                }
            }
            
        } catch (IOException ex) {
            logger.log (Level.SEVERE, "Problem while initializing socket,", ex);
            invokeEvent (EventType.Unreachable);
        }
        
        try {
            channel.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        } try {
            selector.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        }
    }
    
    private void initialize (SocketChannel channel) throws IOException {
        channel.configureBlocking (false) ;
        
        Set<SocketOption<?>> options =channel.supportedOptions();
        
        SocketOption<Integer> soRcvbuf = StandardSocketOptions.SO_RCVBUF;
        if (options.contains(soRcvbuf)) channel.setOption (soRcvbuf, 4*1024) ;
        
        SocketOption<Integer> soSndbuf = StandardSocketOptions.SO_SNDBUF;
        if (options.contains(soSndbuf)) channel.setOption (soSndbuf, 4*1024) ;
    }
    
    void close() {
        isRunning = false;
    }
    
    /**
     * Finalize and end server
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        isRunning = false ;
        invokeEvent(EventType.Disconnected);
        try {
            channel.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        } try {
            selector.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        }
        super.finalize();
    }
    
    protected void reRegister (int value) throws ClosedChannelException {
        channel.register (selector, value);
    }
    
    abstract protected void process (Message readFromSocket);
    abstract protected void invokeEvent (EventType type);
}
