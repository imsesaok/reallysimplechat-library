/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package really.simple.chat.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketOption;
import java.net.StandardSocketOptions;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import really.simple.chat.data.DataManager;
import really.simple.chat.data.UserData;
import really.simple.chat.data.message.Message;
import really.simple.chat.data.message.MessageType;
import really.simple.chat.event.EventType;

/**
 * RSC Base Server Socket. Contains the core of the server (the socket loop, data management, etc.).
 * @author Graphene
 */
public abstract class RSCBaseServerActivity implements Runnable {
    protected SocketAddress address;
    protected Selector selector ;
    protected ServerSocketChannel channel ;
    protected final DataManager manager;
    private final Map <UUID, SocketChannel> users = new HashMap <UUID, SocketChannel> ();
    
    private static final Logger logger =
        Logger.getLogger (RSCServerActivity.class.getName ()) ;
    protected static final UserData serverIdentity = new UserData () ;
    
    protected final ReadWriter rw ;
    
    boolean isRunning = false ;
    
    public RSCBaseServerActivity (SocketAddress address, ReadWriter rw) {
        this.address = address ;
        this.rw = rw ;
        manager = DataManager.getInstance ();
    }
    
    public RSCBaseServerActivity (int port, ReadWriter rw) {
        this (new InetSocketAddress (port), rw) ;
    }
    
    @Override
    public void run () { // The main loop.
        try {
            channel = ServerSocketChannel.open () ;
            selector = Selector.open () ;
            
            initialize (channel) ;
            channel.register (selector, SelectionKey.OP_ACCEPT) ;
            channel.bind (address) ;
            
            isRunning = true ;
            
            while (isRunning) {
                try {
                    selector.selectNow () ;
                    for (SelectionKey key : selector.selectedKeys ()) {
                        selector.selectedKeys ().remove (key) ;
                        
                        if (! key.isValid ()) continue;
                        
                        if (key.isAcceptable ()) {
                            SocketChannel client = channel.accept();
                            if (accept (client, selector)) {
                                logger.log(Level.INFO, "User Entered");
                                UserData user = new UserData ();
                                users.put(user.getId(), client);
                                manager.put(user);
                                writeNow (selector, new Message(new Date(), serverIdentity, MessageType.RN, user), user);
                            }
                        } if (key.isReadable ()) {
                            SocketChannel client = (SocketChannel) key.channel () ;
                            Message data = rw.read (client) ;
                            if (data == null) {
                                logger.log (Level.WARNING, "User exited without announcing.") ;
                                invokeEvent (EventType.Disconnected);
                            } else processAndWrite (data) ;
                        } if (key.isWritable ()) {
                            SocketChannel client = (SocketChannel) key.channel () ;
                            if (rw.rewrite (client)) client.register (selector, SelectionKey.OP_READ) ;
                        }
                    }
                } catch (IOException ex) {
                    logger.log (Level.SEVERE, "Problem operating server.", ex) ;
                }
            }
        } catch (IOException ex) {
            logger.log (Level.SEVERE, "Problem initializing server.", ex) ;
        }
        
        
        try {
            channel.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        } try {
            selector.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        }
    }
    
    private void initialize (ServerSocketChannel channel) throws IOException {
        channel.configureBlocking (false) ;
        
        Set<SocketOption<?>> options =channel.supportedOptions();
        
        SocketOption<Integer> soRcvbuf = StandardSocketOptions.SO_RCVBUF;
        if (options.contains(soRcvbuf)) channel.setOption (soRcvbuf, 4*1024) ;
        
        SocketOption<Integer> soSndbuf = StandardSocketOptions.SO_SNDBUF;
        if (options.contains(soSndbuf)) channel.setOption (soSndbuf, 4*1024) ;
    }
    
    protected void reRegister (SocketChannel channel, int value) throws ClosedChannelException {
        channel.register (selector, value) ;
    }
    
    void close() {
        isRunning = false;
    }
    
    protected void writeToEveryone (Selector selector, Message data) throws IOException {
        for (UserData user : manager.getAllUsers()) {
            send (user.getId(), data) ;
        }
    }
    
    public void send (UUID id, Message data) throws IOException {
        SocketChannel client = users.get (id) ;
        if (! rw.write (client, data)) {
            client.register (selector, SelectionKey.OP_WRITE) ;
        }
    }
    
    public void writeNowToEveryone (Selector selector, Message data) {
        writeNow (selector, data, manager.getAllUsers());
    }
    
    public void writeNow (Selector selector, Message data, UserData... receipient) {
        writeNow (selector, data, Arrays.asList(receipient));
    }
    
    public void writeNow (Selector selector, Message data, List<UserData> receipient) {
        for (UserData user : receipient) {
            UUID id = user.getId();
            SocketChannel client = users.get (id) ;
            rw.addForFuture (client, data);
            try {
                client.register (selector, SelectionKey.OP_WRITE) ;
            } catch (ClosedChannelException e) {
                users.remove(id);
                manager.delete(user);
                invokeEvent (EventType.Disconnected);
            }
        }
    }

    /**
     * Finalize and end server
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        isRunning = false ;
        invokeEvent (EventType.Disconnected);
        try {
            channel.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        } try {
            selector.close () ;
        } catch (IOException ex) {
            logger.log (Level.SEVERE, null, ex) ;
        }
        super.finalize();
    }
    
    abstract boolean accept (SocketChannel client, Selector selector) throws IOException ;
    abstract protected void processAndWrite (Message readFromSocket) throws IOException ;
    abstract protected void invokeEvent (EventType type);
    
}
