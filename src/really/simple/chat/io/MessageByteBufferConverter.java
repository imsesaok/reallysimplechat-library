/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package really.simple.chat.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import really.simple.chat.data.message.Message;

/**
 *
 * @author owner
 */


public class MessageByteBufferConverter {
    
    public static Message toMessage (ByteBuffer buffer) {
        try {
            ByteArrayInputStream byteArrayInput = new ByteArrayInputStream (buffer.array ()) ;
            ObjectInputStream objectInput = new ObjectInputStream (byteArrayInput) ;
            return (Message) objectInput.readObject ();
        } catch (IOException ex) {
            Logger.getLogger (MessageByteBufferConverter.class.getName())
                    .log (Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger (MessageByteBufferConverter.class.getName())
                    .log (Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static ByteBuffer toByteBuffer (Message message) {
        ObjectOutputStream objectOutput = null ;
        try {
            ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream () ;
            objectOutput = new ObjectOutputStream (byteArrayOutput);
            objectOutput.writeObject (message) ;
            objectOutput.flush () ;
            ByteBuffer buffer = ByteBuffer.wrap (byteArrayOutput.toByteArray ()) ;
            return buffer;
        } catch (IOException ex) {
            Logger.getLogger (MessageByteBufferConverter.class.getName())
                    .log (Level.SEVERE, null, ex);
        } finally {
            try {
                objectOutput.close ();
            } catch (IOException ex) {
                Logger.getLogger (MessageByteBufferConverter.class.getName())
                        .log (Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
