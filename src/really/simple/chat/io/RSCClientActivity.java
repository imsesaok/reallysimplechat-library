/*
 * Copyright (C) 2015 Graphene
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package really.simple.chat.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;
import java.util.EnumMap;
import java.util.List;

import really.simple.chat.event.ActionListener;
import really.simple.chat.data.UserData;
import really.simple.chat.data.message.Message;
import really.simple.chat.data.message.MessageType;
import really.simple.chat.event.EventListener;
import really.simple.chat.event.EventType;

/**
 *
 * @author owner
 */
public class RSCClientActivity extends RSCBaseClientActivity {
    final Map <MessageType, List<ActionListener>> listenerMap;
    final Map <EventType, List<EventListener>> eventListenerMap;
    private static final Logger logger = Logger.getLogger (RSCClientActivity.class.getName());
    private UserData dataInstance;
    
    public RSCClientActivity(String ip, int port) {
        this (new InetSocketAddress (ip, port));
    }
    
    public RSCClientActivity (SocketAddress address) {
        super (address, new ReadWriter());
        listenerMap = new EnumMap <MessageType, List<ActionListener>> (MessageType.class);
        eventListenerMap = new EnumMap <EventType, List<EventListener>> (EventType.class);
        
        addMessageListener (MessageType.RN, new ActionListener () {
            public void process(Message msg) {
                dataInstance = (UserData) msg.getContent();
            }            
        });
    }
    
    @Override
    protected void process (Message message) {
        if (listenerMap.containsKey (message.getMessageType ())) {
            for (ActionListener listener : listenerMap.get (message.getMessageType ())) {
                if (listener != null) {
                    listener.process (message);
                }
            }
        }
    }
    
    public void write (Message message) throws IOException {
        rw.write (channel, message);
    }
    
    public void writeNow (Message message) throws ClosedChannelException, IOException {
        rw.addForFuture (channel, message);
        reRegister (SelectionKey.OP_WRITE);
    }
    
    public final void addMessageListener (MessageType type, ActionListener... listeners) {
        List <ActionListener> listenerList = listenerMap.get (type);
        if (listenerList == null)
            listenerList = new ArrayList <ActionListener> ();
        listenerList.addAll (Arrays.asList (listeners));
        listenerMap.put (type, listenerList);
    }
    
    public final void addEventListener (EventType type, EventListener... listeners){
        List <EventListener> listenerList = eventListenerMap.get (type);
        if (listenerList == null)
            listenerList = new ArrayList <EventListener> ();
        listenerList.addAll (Arrays.asList (listeners));
        eventListenerMap.put (type, listenerList);
    };
    
    public final List <ActionListener> getMessageListener (MessageType type) {
        return listenerMap.get (type) ;
    }
    
    public final List <EventListener> getEventListener (EventType type) {
        return eventListenerMap.get (type) ;
    }
    
    public List <ActionListener> removeAllListeners (MessageType type) {
        return listenerMap.remove (type);
    }
    
    public final List <EventListener> removeAllEventListener (EventType type) {
        return eventListenerMap.remove (type);
    }
    
    public final boolean removeMessageListener (MessageType type, ActionListener... listeners) {
        List <ActionListener> listenerList = listenerMap.get (type);
        boolean succeed = false;
        if (listenerList != null) {
            listenerList = new ArrayList <ActionListener> ();
            succeed = listenerList.removeAll (Arrays.asList (listeners));
            listenerMap.put (type, listenerList);
        }
        return succeed;
    }
    
    public final boolean removeEventListener (EventType type, EventListener... listeners) {
        List <EventListener> listenerList = eventListenerMap.get (type);
        boolean succeed = false;
        if (listenerList != null) {
            listenerList = new ArrayList <EventListener> ();
            succeed = listenerList.removeAll (Arrays.asList (listeners));
            eventListenerMap.put (type, listenerList);
        }
        return succeed;
    }

    @Override
    protected void invokeEvent(EventType type) {
        if (eventListenerMap.containsKey (type)) {
            for (EventListener listener : eventListenerMap.get(type)) {
                if (listener != null) {
                    listener.process();
                }
            }
        }
    }
    
    public UserData getUserDataInstance () {
        if (dataInstance == null) {
            dataInstance = new UserData ();
        }
        return dataInstance;
    }
    
    public void setUserData (UserData data) {
        dataInstance = data;
    }
    
    public void setUserName (String name) {
        getUserDataInstance().setName(name);
    }
}
